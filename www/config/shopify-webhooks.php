<?php

declare(strict_types = 1);

use Spurit\ShopifyWebhooks\Services;
use App\Models\Shop\Shop;
use App\Jobs;

return [
    //
    'log'      => [
        'receiving'  => env('APP_DEBUG', false),
        'processing' => env('APP_DEBUG', false)
    ],

    //
    'proxy' => 'https://webhooks-proxy.spur-i-t.com/',

    //
    'handlers' => [
        Services\Webhooks::APP_UNINSTALLED => new Services\WebhookHandler(
            function (Shop $shop, \stdClass $data): void {
                dispatch(new Jobs\ShopUninstall($shop));
            },
            false,
            false
        ),
        Services\Webhooks::THEMES_PUBLISH => new Services\WebhookHandler(
            function (Shop $shop, \stdClass $data): void {
                dispatch(new Jobs\ShopUpdateTheme($shop));
            },
            false,
            false
        )
    ]
];
