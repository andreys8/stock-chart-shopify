<?php

use Spurit\ShopifyApi\Api;
use App\Models\Shop;
use App\Jobs;

$config = [
    'app_id'    => env('APP_ID'),
    'namespace' => env('APP_NAMESPACE'),
    'scopes'    => [
        Api::SCOPE_WRITE_THEMES,
        Api::SCOPE_READ_CONTENT,
        Api::SCOPE_WRITE_CONTENT,
        Api::SCOPE_READ_PRODUCTS,
        Api::SCOPE_WRITE_PRODUCTS,
        Api::SCOPE_READ_THEMES,
        Api::SCOPE_WRITE_THEMES,
        Api::SCOPE_READ_PRODUCT_LISTINGS,
        Api::SCOPE_READ_COLLECTION_LISTINGS,
        Api::SCOPE_READ_CUSTOMERS,
        Api::SCOPE_WRITE_CUSTOMERS,
        Api::SCOPE_READ_ORDERS,
        Api::SCOPE_WRITE_ORDERS,
        Api::SCOPE_READ_DRAFT_ORDERS,
        Api::SCOPE_WRITE_DRAFT_ORDERS,
        Api::SCOPE_READ_SCRIPT_TAGS,
        Api::SCOPE_WRITE_SCRIPT_TAGS,
        Api::SCOPE_READ_FULFILLMENTS,
        Api::SCOPE_WRITE_FULFILLMENTS,
        Api::SCOPE_READ_SHIPPING,
        Api::SCOPE_WRITE_SHIPPING,
        Api::SCOPE_READ_ANALYTICS,
        Api::SCOPE_READ_USERS,
        Api::SCOPE_WRITE_USERS,
        Api::SCOPE_READ_CHECKOUTS,
        Api::SCOPE_WRITE_CHECKOUTS,
        Api::SCOPE_READ_REPORTS,
        Api::SCOPE_WRITE_REPORTS,
        Api::SCOPE_READ_PRICE_RULES,
        Api::SCOPE_WRITE_PRICE_RULES,
        Api::SCOPE_READ_MARKETING_EVENTS,
        Api::SCOPE_WRITE_MARKETING_EVENTS,
        Api::SCOPE_READ_RESOURCE_FEEDBACKS,
        Api::SCOPE_WRITE_RESOURCE_FEEDBACKS
    ],
    'oauth'     => [
        'api_key'       => env('OAUTH_API_KEY'),
        'client_secret' => env('OAUTH_CLIENT_SECRET'),
    ],
    'pc'        => [
        'cookie_name'      => env('PC_COOKIE_NAME'),
        'cookie_key'       => env('PC_COOKIE_KEY', "GZNYBWF\0"),
        'cookie_installed' => env('PC_COOKIE_INSTALLED', ''),
        'url'              => env('PC_URL'),
        'api_url'          => env('PC_API_URL'),
        'api_key'          => env('PC_API_KEY'),
        'api_secret'       => env('PC_API_SECRET'),
        'mock_path'        => env('PC_MOCK_PATH', '/pc-mock'),
        'env'              => env('PC_ENV')
    ],
    'mix'       => [
        'app_name'  => env('MIX_APP_NAME'),
        'app_short' => env('MIX_APP_NAME_SHORT')
    ],
    'embedded'  => env('EMBEDDED_MODE', true),

    'non_free'    => env('PAID_MODE', false),
    'payment_url' => '/payment',

    'shop_model'      => Shop\Shop::class,

    'on_install_jobs' => [
        [
            'job'    => Jobs\ShopInstall::class,
            'queue'  => null,
            'inline' => false
        ]
    ]
];
$config['pc']['admin_cookie_name'] = 'admin-app-logged-' . $config['app_id'];
return $config;
