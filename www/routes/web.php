<?php

use App\Http\Controllers;
use App\Providers\RouteServiceProvider;

Route::middleware('auth')->get('/', function () {
    return redirect('/admin');
});

Route::middleware('auth')
    ->prefix('/admin')
    ->group(function () {
        Route::get('/', Controllers\BaseController::class . '@main');
        Route::prefix('/settings')->group(function () {
            Route::post('/save', Controllers\BaseController::class . '@saveSettings');
        });
        Route::prefix('/products')->group(function () {
            Route::get('/get', Controllers\BaseController::class . '@getProductSettings');
            Route::post('/save', Controllers\BaseController::class . '@saveProducts');
        });
    });

Route::middleware('auth')->get('/payment', Controllers\BaseController::class . '@main');
