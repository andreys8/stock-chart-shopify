const mix = require('laravel-mix');

mix.webpackConfig({
  module: {
    rules: [
      { test: /\.scss$/, loaders: ["sass-loader"] },
      {
        test: /\.jsx?$/,
        exclude: /(node_modules)/,
        use: {
          loader: 'babel-loader',
          options: {
            // cacheDirectory: true //uncomment it to improve speed
          }
        }
      }
    ]
  }
});

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// App admin assets
mix.react('resources/assets/js/app.js', 'public/assets/app.js')
  .sass('resources/assets/sass/app.scss', 'public/assets/app.css');

// Shop frontend assets
mix.js('resources/cloud/js/load.js', 'public/cloud/common.js')
  .sass('resources/cloud/css/common.scss', 'public/cloud/common.css');