<?php

namespace App\Jobs;

use App\Models\Option\Option;
use App\Models\Shop\Shop;
use App\Services\ShopifyAssets\AssetsService;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Log;
use Exception;
use Spurit\ShopifyApi\Api;

class UninstallApp implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var Shop
     */
    private $shop;

    /**
     * UninstallApp constructor.
     *
     * @param Shop $shop
     */
    public function __construct(Shop $shop)
    {
        $this->shop = $shop;
    }

    /**
     * @param Api $api
     */
    public function handle(Api $api)
    {
        $shop = $this->shop;
        $assetsService = new AssetsService($api->setCredentials($shop->domain, $shop->token));
        try {
            $assetsService->excludeAndDelete();
            Log::info(sprintf("SHOP: %s, Snippet was excluded and deleted", $shop->id));
        } catch (Exception $e) {
            Log::error(sprintf("SHOP: %s, Snippet was NOT excluded and deleted, Message: %s", $shop->id, $e->getMessage()));
        }
    }
}
