<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Mingalevme\Illuminate\UQueue\Jobs\Uniqueable;

use Spurit\PluginCenterApi\ApiInterface as PCInterface;
use Spurit\RestProxyConnector\RestProxyConnector;
use App\Services\AmazonService;
use App\Models\Shop;

/**
 * Class ShopUninstall
 * @package App\Jobs
 * @author Sinkevich Alexey
 */
class ShopUninstall implements ShouldQueue, Uniqueable
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var int
     */
    public $tries = 3;

    /**
     * @var int
     */
    public $timeout = 60;

    /**
     * @var Shop\Shop
     */
    private $shop;

    /**
     * ShopUninstall constructor.
     * @param Shop\Shop $shop
     */
    public function __construct(Shop\Shop $shop)
    {
        $this->shop = $shop;
    }

    /**
     * @return string
     */
    public function uniqueable(): string
    {
        return md5($this->shop->id);
    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Exception
     */
    public function handle()
    {
        //$this->unregisterProxyConnector();

        /**
         * @var AmazonService $amazonService
         */
        $amazonService = app(AmazonService::class);
        $amazonService->removeSettings($this->shop);

        /**
         * @var PCInterface $pcApi
         */
        $pcApi = app(PCInterface::class);
        $pcApi->auth()->setUserId($this->shop->user_id);
        $pcApi->instance()->delete();

        $this->shop->delete();

        \Log::info(sprintf(
            'App uninstalled from shop user ID %s',
            $this->shop->user_id
        ));
    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function unregisterProxyConnector(): void
    {
        /**
         * @var RestProxyConnector $restProxyConnector
         */
        $restProxyConnector = app(RestProxyConnector::class);
        $restProxyConnector->unregisterApp(
            intval(config('core.app_id')),
            $this->shop->getHash()
        );
    }
}
