<?php

namespace App\Jobs;

use App\Models\Shop\Shop;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Mingalevme\Illuminate\UQueue\Jobs\Uniqueable;
use Throwable;

class UpdateSupervisor implements ShouldQueue, Uniqueable
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var Carbon
     */
    private $now;

    private const PREFIX = 'shop_';
    private const SUPERVISOR_DIRECTORY = 'supervisor';
    private const LOG_PATH = '/../.tmp/log/supervisor';

    /**
     * UpdateSupervisor constructor.
     */
    public function __construct()
    {
        $this->now = Carbon::now()->format("Y-m-d H:i:s");
    }

    /**
     * @return string
     */
    public function uniqueable(): string
    {
        return md5($this->now);
    }

    /**
     * @param Shop $shop
     *
     * @return bool
     * @throws Throwable
     */
    public function handle(Shop $shop)
    {
        $shopIDs = $shop->all()->pluck('id')->toArray();
        $globPath = sprintf('%s/conf/*.conf', base_path(self::SUPERVISOR_DIRECTORY));
        $configs = glob($globPath);
        foreach ($configs as $config) {
            unlink($config);
        }
        $logs = $this->getLogFiles();

        $user = shell_exec('whoami');

        foreach ($shopIDs as $id) {
            $view = view('shops.supervisor_config',
                [
                    'shop_id' => $id,
                    'base_path' => base_path(),
                    'queue_high' => Shop::QUEUE_PREFIX['high'] . $id,
                    'queue_default' => Shop::QUEUE_PREFIX['default'] . $id,
                    'logs_path' => base_path() . self::LOG_PATH,
                    'user' => $user
                ]);
            $conf = $view->render();
            $fileName = self::PREFIX . $id;
            $configPath = sprintf('%s/conf/%s.conf', base_path(self::SUPERVISOR_DIRECTORY), $fileName);
            file_put_contents($configPath, $conf);
            unset($logs[$id . '']);
        }

        if (trim($user) === 'www-data' || trim($user) === 'vagrant') {
            if (!empty($logs)) {
                $logs = implode(' ', $logs);
                $clearLogFile = base_path(self::SUPERVISOR_DIRECTORY) . '/clear_log.sh';
                $content = sprintf('rm -f %s %s', $logs, $clearLogFile);
                $mask = umask(0);
                file_put_contents($clearLogFile, $content);
                chmod($clearLogFile, 0775);
                umask($mask);
                $exec = sprintf('sudo %s', $clearLogFile);
                shell_exec($exec);
            }
            $exec = sprintf('sudo %s/supervisor_restart.sh', base_path(self::SUPERVISOR_DIRECTORY));
            shell_exec($exec);
        }

        return true;
    }

    /**
     * @return array
     */
    private function getLogFiles(): array
    {
        $logPath = base_path() . self::LOG_PATH;
        $output = [];
        if (is_dir($logPath)) {
            $files = scandir($logPath);
            foreach ($files as $file) {
                $filePath = $logPath . '/' . $file;
                $fileName = pathinfo($filePath, PATHINFO_FILENAME);
                $fileName = explode('_', $fileName);
                if (sizeof($fileName) === 2) {
                    $prefix = reset($fileName);
                    $id = end($fileName);
                    if ($prefix === rtrim(self::PREFIX, '_')) {
                        $output[$id] = $filePath;
                    }
                }
            }
            unset($file);
        }
        return $output;
    }
}
