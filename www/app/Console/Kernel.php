<?php

namespace App\Console;

use App\Console\Commands\Supervisor;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Supervisor\ClearLogs::class,
        Supervisor\Update::class
    ];

    /**
     * @var string
     */
    private $logFile = __DIR__ . '/../../storage/logs/cron.log';

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command(Supervisor\ClearLogs::class)->dailyAt('02:00')->withoutOverlapping()->appendOutputTo($this->logFile);
        // $schedule->command('inspire')
        //          ->hourly();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');
        require base_path('routes/console.php');
    }
}
