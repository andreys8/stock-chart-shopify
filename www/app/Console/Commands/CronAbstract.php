<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Spurit\PluginCenterApi\ApiInterface as PluginCenterInterface;
use Spurit\PluginCenterApi\Resource\Crons;

/**
 * Class CronAbstract
 *
 * @package App\Console\Commands
 * @author  Sinkevich Alexey
 */
abstract class CronAbstract extends Command
{
    /**
     * @var Crons
     */
    private $pluginCenterCron;

    /**
     * @var string
     */
    private $cronHash = '';

    /**
     * @var string
     */
    private $cronKey = '';

    /**
     * CronAbstract constructor.
     *
     * @param PluginCenterInterface $pcApi
     */
    public function __construct(PluginCenterInterface $pcApi)
    {
        $this->pluginCenterCron = $pcApi->crons();
        parent::__construct();
    }

    /**
     *
     */
    protected function logCronStart(): void
    {
        $this->pluginCenterCron->start($this->getCronHash(), $this->getCronKey());
    }

    /**
     * @return string
     */
    private function getCronHash(): string
    {
        if (!$this->cronHash) {
            $this->cronHash = md5(time());
        }
        return $this->cronHash;
    }

    /**
     * @return string
     */
    private function getCronKey(): string
    {
        if (!$this->cronKey) {
            $this->cronKey = config('core.mix.app_name') . '-' . $this->getCommand();
        }
        return $this->cronKey;
    }

    /**
     *
     */
    protected function logCronStop(): void
    {
        $this->pluginCenterCron->stop($this->getCronHash(), $this->getCronKey());
    }

    /**
     * @return string
     */
    private function getCommand(): string
    {
        $arguments = $this->arguments();
        unset($arguments[0]);
        $arguments = collect($arguments)->implode(' ');
        $options = collect($this->options())->filter(function ($item) {
            return $item;
        })->map(function ($item, $key) {
            $return = '--' . $key;
            if ($item !== true) {
                $return .= '=' . $item;
            }
            return $return;
        })->implode(' ');
        return trim($arguments . ' ' . $options);
    }
}