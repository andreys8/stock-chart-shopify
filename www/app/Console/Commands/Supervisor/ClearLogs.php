<?php

namespace App\Console\Commands\Supervisor;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;

class ClearLogs extends Command
{
    /**
     * @var string
     */
    protected const LOG_DIRECTORY = 'logs';

    private const SUPERVISOR_DIRECTORY = 'supervisor';

    /**
     * @var int
     */
    protected const DAYS = 2;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'supervisor:clear';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command for clearing Supervisor log-files';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return int
     */
    public function handle(): int
    {
        $logs = File::allFiles(storage_path(self::LOG_DIRECTORY));
        foreach ($logs as $key => $log) {
            $updateDate = Carbon::createFromTimestamp($log->getMTime());
            $days = $updateDate->diffInDays(Carbon::now());
            if ($days >= self::DAYS) {
                $logs[$key] = $log->getRealPath();
            } else {
                unset($logs[$key]);
            }
        }
        unset($log);
        if (!empty($logs)) {
            $logs = implode(' ', $logs);
            $clearLogFile = base_path(self::SUPERVISOR_DIRECTORY) . '/clear_old_log.sh';
            $content = sprintf('rm -f %s %s', $logs, $clearLogFile);
            $mask = umask(0);
            file_put_contents($clearLogFile, $content);
            chmod($clearLogFile, 0775);
            umask($mask);
            $exec = sprintf('sudo %s', $clearLogFile);
            shell_exec($exec);
        }
        return 1;
    }
}