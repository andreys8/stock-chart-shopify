<?php

namespace App\Services;

use App\Models\Shop\Shop;
use Spurit\Core\Services\S3;

/**
 * Class AmazonService
 * @package App\Http\Services
 * @author Igor Razumovsky <igor.r@spur-i-t.com>
 */
class AmazonService
{
    /**
     * @var S3
     */
    private $amazonS3;

    /**
     * AmazonService constructor.
     * @param S3 $amazonS3
     */
    public function __construct(S3 $amazonS3)
    {
        $this->amazonS3 = $amazonS3;
    }

    /**
     * @param Shop $shop
     * @throws \Throwable
     */
    public function saveSettings(Shop $shop): void
    {
        $this->amazonS3->put($shop->amazonPath('js'), $shop->settings->getJsData());
        $this->amazonS3->put($shop->amazonPath('css'), $shop->settings->getCssData());
    }

    /**
     * @param Shop $shop
     */
    public function removeSettings(Shop $shop): void
    {
        $this->amazonS3->delete($shop->amazonPath('js'));
        $this->amazonS3->delete($shop->amazonPath('css'));
    }
}
