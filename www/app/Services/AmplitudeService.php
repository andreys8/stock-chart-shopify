<?php
namespace App\Services;

use Spurit\PluginCenterApi\ApiInterface as PluginCenterInterface;

/**
 * Class AmplitudeService.
 * @package App\Http\Services
 * @author Igor Razumovsky <igor.r@spur-i-t.com>
 */
class AmplitudeService
{
    /**
     * @var PluginCenterInterface
     */
    private $pcApi;

    /**
     * AmplitudeService constructor.
     * @param PluginCenterInterface $pcApi
     */
    public function __construct(PluginCenterInterface $pcApi)
    {
        $this->pcApi = $pcApi;
    }

    /**
     * @param string $eventType
     * @param int $userId
     * @param array $eventProperties
     */
    public function sendEvent(string $eventType, int $userId, array $eventProperties): void
    {
        $eventProperties['eventName'] = $eventType;

        $this->pcApi->auth()->setUserId($userId);
        $this->pcApi->events()->post($eventProperties);
    }
}