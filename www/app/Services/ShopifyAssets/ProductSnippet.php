<?php

declare(strict_types=1);

namespace App\Services\ShopifyAssets;

use Illuminate\Contracts\View\Factory as ViewFactory;
use App\Models\Shop\Shop;

/**
 * Class ProductSnippet
 * @package App\Services\ShopifyAssets
 */
class ProductSnippet extends AssetAbstract
{
    /**
     * @var ViewFactory
     */
    private $viewFactory;

    /**
     * ProductSnippet constructor.
     * @param ViewFactory $viewFactory
     */
    public function __construct(ViewFactory $viewFactory)
    {
        $this->viewFactory = $viewFactory;
    }

    /**
     * @return string
     */
    public function placeSearchText(): string
    {
        return '';
    }

    /**
     * @return string
     */
    public function targetTemplate(): string
    {
        return 'templates/product.liquid';
    }

    /**
     * @param Shop $shop
     * @return string
     */
    public function render(Shop $shop): string
    {
        return (string)($this->viewFactory->make(
            'snippet.product-snippet',
            [
                'shopHash' => $shop->getHash(),
                'appName'  => config('core.mix.app_name')
            ]
        ));
    }
}
