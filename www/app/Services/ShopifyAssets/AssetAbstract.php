<?php

declare(strict_types=1);

namespace App\Services\ShopifyAssets;

use App\Models\Shop\Shop;

/**
 * Class AssetAbstract
 * @package App\Services\ShopifyAssets
 * @author Sinkevich Alexey
 */
abstract class AssetAbstract
{
    /**
     * If false, rendered code will be directly inserted into the target template
     * @return bool
     */
    public function isSnippet(): bool
    {
        return true;
    }

    /**
     * @return string
     */
    public function placePosition(): string
    {
        return AssetsService::PLACE_BEFORE;
    }

    /**
     * @return string
     */
    public function placeSearchText(): string
    {
        return '';
    }

    /**
     * Must return something like 'layout/theme.liquid'
     * Or leave an empty string if you want just upload snippet without any includes
     * @return string
     */
    public function targetTemplate(): string
    {
        return '';
    }

    /**
     * @param Shop $shop
     * @return string
     */
    abstract public function render(Shop $shop): string;
}