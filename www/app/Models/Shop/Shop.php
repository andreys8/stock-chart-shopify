<?php

namespace App\Models\Shop;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Spurit\Core\Model\Shop as BaseShop;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\HasOne;
use App\Models;

/**
 * Class Shop
 * @package App\Models\Shop
 *
 * @property int $id
 * @property int $user_id
 * @property string $domain
 * @property string $domain_front
 * @property string $name
 * @property string $email
 * @property string $token
 * @property string $currency
 * @property string $money_format
 * @property string $timezone
 * @property string $hash
 * @property boolean $active
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Carbon $deleted_at
 * @property boolean $installed
 *
 * @property Models\Settings $settings
 * @property Models\ProductSettings $productSettings
 *
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class Shop extends BaseShop
{
    //use SoftDeletes;

    public const QUEUE_PREFIX = [
        'high' => 'shop_high_',
        'default' => 'shop_default_'
    ];

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @return string
     */
    public function queueHigh()
    {
        return self::QUEUE_PREFIX['high'] . $this->id;
    }

    /**
     * @return string
     */
    public function queueDefault()
    {
        return self::QUEUE_PREFIX['default'] . $this->id;
    }

    /**
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     *
     */
    public static function boot(): void
    {
        parent::boot();
        self::created(function (self $shop) {
            if (!$shop->settings) {
                $shop->settings()->create();
            }
        });
    }


    public function settings(): HasOne
    {
        return $this->hasOne(Models\Settings::class, 'shop_id');
    }



    public function productSettings(): HasMany
    {
        return $this->HasMany(Models\ProductSettings::class, 'shop_id');
    }
}
