<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Relation;

/**
 * Class RelationshipsTrait
 * @package App\Models\Traits
 * @author Sinkevich Alexey
 */
trait RelationshipsTrait
{
    /**
     * @param string $filterRelationClass
     * @return array
     * @throws \ReflectionException
     */
    public function relationships($filterRelationClass = ''): array
    {
        $class = get_class($this);
        $relationships = [];
        foreach ((new \ReflectionClass($this))->getMethods(\ReflectionMethod::IS_PUBLIC) as $method) {
            if ($method->class !== $class || !empty($method->getParameters()) || $method->getName() === __FUNCTION__) {
                continue;
            }

            $type = (string)$method->getReturnType();
            if ($type === Relation::class || is_subclass_of($type, Relation::class)) {
                $relationships[$method->getName()] = $type;
            }
        }
        if (!$filterRelationClass) {
            return $relationships;
        }

        $filtered = [];
        foreach ($relationships as $method => $type) {
            if ($type === $filterRelationClass) {
                $filtered[] = $method;
            }
        }
        return $filtered;
    }
}
