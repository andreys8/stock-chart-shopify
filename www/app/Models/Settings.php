<?php

namespace App\Models;

use Carbon\Carbon;
use App\Models;
use App\Events;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use \Throwable;

/**
 * Class Settings
 * @package App\Models
 * @author Sinkevich Alexey
 *
 * @property int $shop_id
 * @property array $config (json)
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @property Models\Shop\Shop $shop
 *
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class Settings extends Model
{
    /**
     * @var string
     */
    protected $table = 'settings';

    /**
     * @var string
     */
    protected $primaryKey = 'shop_id';

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $casts = [
        'config' => 'array',
    ];

    /**
     * @var array
     */
    private static $configDefault = [
        'selectors' => [],
        'low_quantity'  => 0,
        'high_quantity' => 1,
        'show_real_quantity' => false
    ];

    /**
     *
     */
    public static function boot(): void
    {
        parent::boot();
        self::creating(function (self $setting) {
            $setting->config = $setting->getConfigAttribute(json_encode($setting->config));
        });
        self::created(function (self $setting) {
            event(new Events\OnSettingsSaved($setting->shop));
        });
        self::updated(function (self $setting) {
            if ($setting->isDirty('config')) {
                event(new Events\OnSettingsSaved($setting->shop));
            }
        });
    }

    /**
     * @return array
     */
    public static function getConfigDefault(): array
    {
        return self::$configDefault;
    }

    /**
     * @param null|string $value
     * @return array
     */
    public function getConfigAttribute(?string $value): array
    {
        $value = !is_null($value) ? json_decode($value, true) : [];
        $value = array_replace_recursive(self::$configDefault, $value);
        return $value;
    }

    /**
     * @return BelongsTo
     */
    public function shop(): BelongsTo
    {
        return $this->belongsTo(Models\Shop\Shop::class);
    }

    /**
     * @return string
     */
    public function getJsData(): string
    {
        try {
            $result = view(
                'assets.config',
                [
                    'appName' => config('core.mix.app_name'),
                    'encodedSettings' => json_encode($this->config),
                    'encodedProductSettings' => json_encode($this->shop->productSettings)
                ]
            )->render();
        } catch (Throwable $t) {
            $result = sprintf('console.warning(%s, %s)', __CLASS__ . '->' . __METHOD__ , $t->getMessage());
        }
        return $result;
    }

    /**
     * @return string
     */
    public function getCssData(): string
    {
        try {
            $result = '';
        } catch (Throwable $t) {
            $result = sprintf('/* %s, %s */', __CLASS__ . '->' . __METHOD__ , $t->getMessage());
        }
        return $result;
    }
}
