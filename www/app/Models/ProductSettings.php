<?php

namespace App\Models;

use App\Models;
use App\Events;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class ProductSettings
 * @package App\Models
 * @property $product_id
 * @property $shop_id
 * @property $config
 * @property Models\Shop\Shop $shop
 */
class ProductSettings extends Model
{
    /**
     * @var string
     */
    protected $table = 'product_settings';

    /**
     * @var string
     */
    protected $primaryKey = 'product_id';

    /**
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var array
     */
    protected $casts = [
        'config' => 'json',
    ];

    public static function boot(): void
    {
        parent::boot();
        self::created(function (self $productSettings) {
            event(new Events\OnSettingsSaved($productSettings->shop));
        });
        self::updated(function (self $productSettings) {
            if ($productSettings->isDirty('config')) {
                event(new Events\OnSettingsSaved($productSettings->shop));
            }
        });
    }

    /**
     * @return BelongsTo
     */
    public function shop(): BelongsTo
    {
        return $this->belongsTo(Models\Shop\Shop::class);
    }
}
