<?php

namespace App\Events;

use Illuminate\Queue\SerializesModels;
use App\Models;

class OnInstall
{
    use SerializesModels;

    /**
     * @var Models\Shop\Shop
     */
    public $shop;

    /**
     * OnInstall constructor.
     * @param Models\Shop\Shop $shop
     */
    public function __construct(Models\Shop\Shop $shop)
    {
        $this->shop = $shop;
    }
}
