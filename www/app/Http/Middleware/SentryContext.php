<?php

namespace App\Http\Middleware;

use Closure;
use Sentry\State\Hub;
use Sentry\State\Scope;

class SentryContext
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (app()->bound('sentry')) {
            // Add user context
            if (auth()->check()) {
                Hub::getCurrent()->configureScope(function (Scope $scope): void {
                    $scope->setUser(['user' => auth()->user()->toArray()]);
                });
            }
        }

        return $next($request);
    }
}