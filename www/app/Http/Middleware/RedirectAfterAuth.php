<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;

/**
 * Class RedirectAfterAuth
 * @package App\Http\Middleware
 *
 * @author Ilya Zhernosek
 * @author Sinkevich Alexey
 */
class RedirectAfterAuth
{
    /**
     * @var Guard
     */
    protected $auth;

    /**
     *
     */
    const COOKIE_DISABLE_EMBEDDED = 'disableEmbedded';

    /**
     *
     */
    const SEPARATE_WINDOW_FLAG = 'embedded=0';


    /**
     * Auth constructor.
     * @param Guard $auth
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($this->auth->guest() && !$request->ajax() && self::hasDisableEmbeddedParam()) {
            setcookie(self::COOKIE_DISABLE_EMBEDDED, 1, 0);
        }
        return $next($request);
    }


    /**
     * @return bool
     */
    public static function hasDisableEmbeddedParam(): bool
    {
        return strpos(\URL::full(), self::SEPARATE_WINDOW_FLAG) !== false;
    }
}
