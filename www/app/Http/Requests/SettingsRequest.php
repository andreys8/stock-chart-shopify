<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class SettingsRequest
 * @package App\Http\Requests
 * @property integer high_quantity
 * @property integer low_quantity
 * @property boolean show_real_quantity
 */
class SettingsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'high_quantity' => 'required|integer',
            'low_quantity' => 'required|integer',
            'show_real_quantity' => 'required|boolean'
        ];
    }
}
