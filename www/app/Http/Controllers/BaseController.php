<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductsRequest;
use App\Http\Requests\SettingsRequest;
use App\Models\Shop\Shop;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Spurit\SelectorPicker\SelectorPicker;
use Spurit\ShopifyApi\Api as ShopifyApi;

class BaseController extends Controller
{
    protected const OPTIONS_SIZE_PER_PAGE = 5;

    /**
     * @var Request
     */
    protected $request;

    /**
     * @var SelectorPicker
     */
    protected $selectorPicker;


    /**
     * @var ShopifyApi
     */
    protected $shopifyApi;


    /**
     * BaseController constructor.
     * @param Request $request
     * @param SelectorPicker $selectorPicker
     * @param ShopifyApi $shopifyApi
     */
    public function __construct(Request $request, SelectorPicker $selectorPicker, ShopifyApi $shopifyApi)
    {
        parent::__construct($request);
        $this->selectorPicker = $selectorPicker;
        $this->shopifyApi = $shopifyApi;
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function main()
    {

        $urlParams = [
            'page' => 1
        ];

        \JavaScript::put([
            'initial' => [
                'settings' => $this->shop()->settings->config,
                'urlParams' => $urlParams
            ],
            'config' => [
                'selectorPicker' => [
                    'productPage' => $this->selectorPicker->getProductUrl(),
                ],
            ]
        ]);
        return view('admin');
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function getProductSettings(Request $request): JsonResponse
    {
        $productSettings = $this->shop()->productSettings()->select('product_id', 'config')
            ->paginate(self::OPTIONS_SIZE_PER_PAGE, "*", null, $request->page);
        $productApi = $this->shopifyApi->products();
        $product = [];

        foreach ($productSettings as $productSetting) {
            $currentProduct = $productApi->get([], $productSetting->product_id);
            $currentOption = $productSetting->config;
            $product[] = [
                "product" => reset($currentProduct),
                "productOptions" => $currentOption,
                "lastPage" => $productSettings->lastPage()
            ];
        }
        return response()->json($product, 200);
    }


    /**
     * @param SettingsRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function saveSettings(SettingsRequest $request): JsonResponse
    {
        $status = 400;
        $settings = $this->shop()->settings;
        $settings->config = $request->all();
        if($settings->save()) {
            $status = 200;
        }
        return response()->json($settings, $status);
    }


    /**
     * @param ProductsRequest $request
     * @return JsonResponse
     */
    public function saveProducts(ProductsRequest $request): JsonResponse
    {
        $status = 400;
        $response = [];

        foreach ($request->ids as $id) {
            $response[] = $this->shop()->productSettings()->updateOrCreate(
                ['product_id' => $id],
                ['config' => $request->settings]
            );
        }
        if($response) {
            $status = 200;
        }
        return response()->json($response, $status);
    }


    /**
     * @return Shop
     */
    protected function shop(): Shop
    {
        return $this->request->user();
    }
}
