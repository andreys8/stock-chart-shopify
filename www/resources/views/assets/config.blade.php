<?php
/**
 * @var string $encodedSettings
 * @var string $encodedProductSettings
 * @var string $appName
 */
?>
if(typeof Spurit === 'undefined') var Spurit = {};
if(typeof Spurit.<?=$appName?> === 'undefined') Spurit.<?=$appName?> = {};
Spurit.<?=$appName?>.settings = <?= $encodedSettings ?>;
Spurit.<?=$appName?>.productSettings = <?= $encodedProductSettings ?>;
