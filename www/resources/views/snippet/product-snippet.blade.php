<script>
  if(typeof(Spurit) === 'undefined'){
    var Spurit = {};
  }
  if(!Spurit.{{$appName}}){
    Spurit.{{$appName}} = {};
  }
  if(!Spurit.{{$appName}}.snippet){
    Spurit.{{$appName}}.snippet = {};
  }
  Spurit.{{$appName}}.snippet.shopHash = '{{$shopHash}}';
</script>

<script src="{{config('filesystems.disks.s3.common_path.url')}}common.js"></script>
<link href="{{config('filesystems.disks.s3.common_path.url')}}common.css" rel="stylesheet" type="text/css" media="all">