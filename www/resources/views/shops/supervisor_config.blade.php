[program:shop_<?php echo $shop_id;?>]
command=php <?php echo $base_path;?>/artisan queue:work --queue=<?php echo $queue_high;?>,<?php echo $queue_default;?> --tries=3
autostart=true
autorestart=true
user=<?php echo $user;?>
numprocs=1
redirect_stderr=true
;stdout_logfile=<?php echo $logs_path;?>/shop_<?php echo $shop_id;?>.log