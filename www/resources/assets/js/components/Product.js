'use strict';

import React, {Component} from 'react';
import {Thumbnail, ResourceItem, ResourceList, TextStyle} from '@shopify/polaris';

export default class Product extends Component {
    /**
     * @param props
     */
    constructor(props) {
        super(props);
        this.state = {
            items: props.items,
            shopUrl : props.shopUrl
        };
    }

    /**
     *
     * @returns {*}
     */
    render() {
        const state = this.state;

        return (
            <ResourceList
                items = {state.items}
                renderItem = {(item) => {
                    const {id, handle, title, image} = item;
                    const media = <Thumbnail source={image.src} size={"medium"} alt={title} />;
                    return (
                        <ResourceItem
                            id={id}
                            url={ state.shopUrl + "/products/" + handle }
                            media={media}
                            accessibilityLabel={`View details for ${title}`}
                        >
                            <h3>
                                <TextStyle variation="strong">{title}</TextStyle>
                            </h3>
                        </ResourceItem>
                    );
                }}
            />
        );
    }
}
