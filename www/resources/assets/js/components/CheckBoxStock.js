import React, { Component } from "react";
import { Checkbox } from "@shopify/polaris";

export default class CheckboxExample extends Component {

    /**
     *
     * @param props
     */
    constructor(props) {
        super(props);
        this.onChange = this.props.onChange;
    }

    render() {
        return (
            <Checkbox
                id={"checked"}
                label="Show stock quantity"
                checked={this.props.checked}
                onChange={this.onChange}
            />
        );
    }
}
