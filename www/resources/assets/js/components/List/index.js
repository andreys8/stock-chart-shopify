'use strict';

import React, { Component } from 'react';
import { Card, EmptyState, ResourceList } from '@shopify/polaris';
import * as PropTypes from 'prop-types';
import { Item, Pager } from './components';

export default class List extends Component {

  /**
   *
   * @param props
   */
  constructor (props) {
    super(props);
    this.state = {
      search: props.search,
      loading: props.loading,
    };
    this.onEnabled = props.onEnabled;
    this.onRemove = props.onRemove;
    this.onEdit = props.onEdit;
    this.onPage = props.onPage;
    this.onSearch = props.onSearch;
    this.onLoad = props.onLoad;
    this.onCheck = props.onCheck;
  }

  componentDidMount () {
    this.setState({loading: true});
    this.onLoad(() => {
      this.setState({loading: false});
    });
  }

  /**
   *
   * @returns {{onPreview: onPreview, onSearch: onSearch, resourceName: {plural: string, singular: string}, loading: boolean, emptyState: {image: string, heading: string, secondaryAction: {}, action: {content: string}, text: string}, onEdit: onEdit, search: string, onPage: onPage, pager: {total: number, nextPageUrl: null, currentPage: number, prevPageUrl: null}, onEnabled: onEnabled, onLoad: onLoad, onRemove: onRemove, items: Array}}
   */
  static get defaultProps () {
    return {
      items: [],
      pager: {
        total: 0,
        nextPageUrl: null,
        prevPageUrl: null,
        currentPage: 1,
      },
      loading: false,
      search: '',
      emptyState: {
        heading: 'First of all create',
        text: 'Create your first item and you will see it on the dashboard',
        image: 'https://cdn.shopify.com/s/files/1/0757/9955/files/empty-state.svg',
        action: {
          content: 'Create first'
        },
        secondaryAction: {}
      },
      resourceName: {
        singular: '',
        plural: '',
      },
      onEnabled: () => {},
      onPreview: () => {},
      onRemove: () => {},
      onEdit: () => {},
      onPage: () => {},
      onSearch: () => {},
      onLoad: () => {},
      onCheck: () => true
    };
  }

  /**
   *
   * @returns {{onPreview: Requireable<(...args: any[]) => any>, onSearch: Requireable<(...args: any[]) => any>, resourceName: Requireable<InferProps<{plural: Requireable<string>, singular: Requireable<string>}>>, loading: Requireable<boolean>, emptyState: Requireable<InferProps<{image: Requireable<string>, heading: Requireable<string>, secondaryAction: Requireable<InferProps<{onAction: Requireable<(...args: any[]) => any>, external: Requireable<boolean>, accessibilityLabel: Requireable<string>, content: Requireable<string>, url: Requireable<string>}>>, action: Requireable<InferProps<{onAction: Requireable<(...args: any[]) => any>, external: Requireable<boolean>, accessibilityLabel: Requireable<string>, content: Requireable<string>, url: Requireable<string>}>>, text: Requireable<string>}>>, onEdit: Requireable<(...args: any[]) => any>, search: Requireable<string>, onPage: Requireable<(...args: any[]) => any>, pager: Requireable<InferProps<{total: Requireable<number>, nextPageUrl: Requireable<string>, currentPage: Requireable<number>, prevPageUrl: Requireable<string>}>>, onEnabled: Requireable<(...args: any[]) => any>, onLoad: Requireable<(...args: any[]) => any>, onRemove: Requireable<(...args: any[]) => any>, items: Requireable<((any & Partial<InferPropsInner<Pick<{id: Requireable<number>, title: Requireable<string>, enabled: Requireable<boolean>}, "id" | "title" | "enabled">>>) | undefined | null)[]>}}
   */
  static get propTypes () {
    return {
      items: PropTypes.arrayOf(
        PropTypes.shape({
          id: PropTypes.number,
          title: PropTypes.string,
          enabled: PropTypes.bool
        })),
      pager: PropTypes.shape({
        currentPage: PropTypes.number,
        prevPageUrl: PropTypes.string,
        nextPageUrl: PropTypes.string,
        total: PropTypes.number
      }),
      loading: PropTypes.bool,
      search: PropTypes.string,
      resourceName: PropTypes.shape({
        singular: PropTypes.string,
        plural: PropTypes.string,
      }),
      emptyState: PropTypes.shape({
        heading: PropTypes.string,
        text: PropTypes.string,
        image: PropTypes.string,
        action: PropTypes.shape({
          content: PropTypes.string,
          accessibilityLabel: PropTypes.string,
          url: PropTypes.string,
          external: PropTypes.bool,
          onAction: PropTypes.func
        }),
        secondaryAction: PropTypes.shape({
          content: PropTypes.string,
          accessibilityLabel: PropTypes.string,
          url: PropTypes.string,
          external: PropTypes.bool,
          onAction: PropTypes.func
        })
      }),
      onEnabled: PropTypes.func,
      onRemove: PropTypes.func,
      onPreview: PropTypes.func,
      onSearch: PropTypes.func,
      onEdit: PropTypes.func,
      onPage: PropTypes.func,
      onLoad: PropTypes.func,
      onCheck: PropTypes.func
    };
  }

  /**
   *
   * @param prevProps
   * @param prevState
   * @param snapshot
   */
  componentDidUpdate (prevProps, prevState, snapshot) {
    let {search} = prevProps;
    if (search !== this.props.search) {
      search = this.props.search;
      this.setState({search});
    }
  }

  /**
   *
   * @return {*}
   */
  get searchControl () {
    let state = this.state;
    return (
      <ResourceList.FilterControl
        searchValue={state.search}
        onSearchChange={(search) => {
          this.setState({search});
          if (search.length >= 2 || !search.length) {
            this.setState({loading: true});
            this.onSearch(search, () => {
              this.setState({loading: false});
            });
          }
        }}
      />
    );
  }

  /**
   *
   * @returns {*}
   */
  get emptyState () {
    let emptyState = {...List.defaultProps.emptyState, ...this.props.emptyState};
    return (
      <EmptyState
        heading={emptyState.heading}
        action={emptyState.action}
        secondaryAction={emptyState.secondaryAction}
        image={emptyState.image}
      >
        <p>{emptyState.text}</p>
      </EmptyState>
    );
  }

  /**
   *
   * @return {boolean}
   */
  get next () {
    return this.props.pager.nextPageUrl !== null;
  }

  /**
   *
   * @return {boolean}
   */
  get prev () {
    return this.props.pager.prevPageUrl !== null;
  }

  /**
   *
   * @return {*}
   */
  render () {
    const {state, props} = this;
    if (props.items.length < 1 && state.search.length < 1 && props.pager.currentPage <= 1) {
      return this.emptyState;
    }
    return (
      <Card>
        <ResourceList
          showHeader={true}
          loading={state.loading}
          resourceName={props.resourceName}
          items={props.items}
          filterControl={this.searchControl}
          renderItem={item => (
            <Item
              {...item}
              onRemove={(callBack) => {
                this.onRemove(item, callBack);
              }}
              onEdit={() => {
                this.onEdit(item);
              }}
              onEnabled={(callBack) => {
                this.onEnabled(item, () => {
                  callBack();
                });
              }}
              onCheck={this.onCheck}
            />
          )
          }
        />
        <Pager
          total={props.pager.total}
          current={props.pager.currentPage}
          hasPrevious={this.prev}
          hasNext={this.next}
          onNext={() => {
            let page = this.props.pager.currentPage + 1;
            this.setState({loading: true});
            this.onPage(page, () => {
              this.setState({loading: false});
            });
          }}
          onPrevious={() => {
            let page = this.props.pager.currentPage - 1;
            page = page < 1 ? 1 : page;
            this.setState({loading: true});
            this.onPage(page, () => {
              this.setState({loading: false});
            });
          }}
        />
      </Card>
    );
  }
}