'use strict';

import React, { Component } from 'react';
import * as PropTypes from 'prop-types';
import { Card, Pagination } from '@shopify/polaris';

export default class Pager extends Component {
  /**
   * @param props
   */
  constructor (props) {
    super(props);
    this.state = {};
    this.onPrevious = props.onPrevious;
    this.onNext = props.onNext;
  }

  /**
   *
   * @return {{current: number, hasPrevious: boolean, hasNext: boolean, total: number, onPrevious: onPrevious, onNext: onNext}}
   */
  static get defaultProps () {
    return {
      current: 1,
      hasPrevious: true,
      hasNext: true,
      total: 1,
      onPrevious: () => {},
      onNext: () => {},
    };
  }

  /**
   *
   * @return {{hasPrevious: Requireable<boolean>, hasNext: Requireable<boolean>, current: Requireable<number>, total: Requireable<number>, onPrevious: Requireable<(...args: any[]) => any>, onNext: Requireable<(...args: any[]) => any>}}
   */
  static get propTypes () {
    return {
      hasPrevious: PropTypes.bool.isRequired,
      hasNext: PropTypes.bool.isRequired,
      current: PropTypes.number.isRequired,
      total: PropTypes.number.isRequired,
      onPrevious: PropTypes.func,
      onNext: PropTypes.func,
    };
  }

  /**
   *
   * @return {*}
   */
  render () {
    const props = this.props;
    if (!props.total || (props.total && !props.hasPrevious && !props.hasNext)) {
      return (<Card sectioned/>);
    }
    return (
      <Card sectioned>
        <Pagination
          hasPrevious={props.hasPrevious}
          onPrevious={() => {
            this.onPrevious();
          }}
          hasNext={props.hasNext}
          onNext={() => {
            this.onNext();
          }}
        />
      </Card>
    );
  }
}