'use strict';

import React, { Component } from 'react';
import * as PropTypes from 'prop-types';
import { Avatar, Badge, ResourceList, Stack, TextStyle } from '@shopify/polaris';
import Buttons from './Buttons';

export default class Item extends Component {

  /**
   *
   * @param props
   */
  constructor (props) {
    super(props);
    this.state = {};
    this.onEnabled = props.onEnabled;
    this.onPreview = props.onPreview;
    this.onRemove = props.onRemove;
    this.onEdit = props.onEdit;
    this.onCheck = props.onCheck;
  }

  /**
   *
   * @returns {{onCheck: onCheck, onEdit: onEdit, onEnabled: onEnabled, onPreview: onPreview, id: number, title: string, onRemove: onRemove}}
   */
  static get defaultProps () {
    return {
      id: 0,
      title: '',
      onEnabled: () => {},
      onPreview: () => {},
      onRemove: () => {},
      onEdit: () => {},
      onCheck: () => {}
    };
  }

  /**
   *
   * @returns {{onCheck: Requireable<(...args: any[]) => any>, onEdit: Requireable<(...args: any[]) => any>, onEnabled: Requireable<(...args: any[]) => any>, onPreview: Requireable<(...args: any[]) => any>, id: Validator<NonNullable<number>>, title: Validator<NonNullable<string>>, onRemove: Requireable<(...args: any[]) => any>, enabled: Requireable<boolean>}}
   */
  static get propTypes () {
    return {
      id: PropTypes.number.isRequired,
      title: PropTypes.string.isRequired,
      enabled: PropTypes.bool,
      onEnabled: PropTypes.func,
      onRemove: PropTypes.func,
      onPreview: PropTypes.func,
      onEdit: PropTypes.func,
      onCheck: PropTypes.func
    };
  }

  /**
   *
   * @return {string}
   */
  get initials () {
    const {title} = this.props;
    return title.charAt(0).toUpperCase();
  }

  /**
   *
   * @return {boolean|Requireable<boolean>|*}
   */
  get status () {
    const props = this.props;
    return props.hasOwnProperty('enabled') && (
      <Badge status={props.enabled ? 'success' : 'default'}>
        {props.enabled ? 'Active' : 'Inactive'}
      </Badge>
    );
  }

  /**
   *
   * @returns {boolean}
   */
  get canBeDelete () {
    const props = this.props;
    return this.onCheck(props);
  }

  /**
   *
   * @return {*}
   */
  render () {
    const props = this.props,
      media = <Avatar size="medium" name={props.title} initials={this.initials}/>;
    return (
      <ResourceList.Item
        id={props.id}
        url={null}
        onClick={this.onEdit}
        media={media}
      >
        <Stack alignment="center">
          <Stack.Item fill>
            <div onClick={() => {this.onEdit();}}>
              <h3>
                <TextStyle variation="strong">{props.title}</TextStyle>
              </h3>
            </div>
          </Stack.Item>
          <Stack.Item>
            {this.status}
          </Stack.Item>
          <Stack.Item>
            <Buttons
              enabled={props.enabled}
              visible={{
                enabled: props.hasOwnProperty('enabled'),
                remove: this.canBeDelete
              }}
              onEdit={this.onEdit}
              onEnabled={(callBack) => {
                this.onEnabled(() => {
                  callBack();
                });
              }}
              onRemove={this.onRemove}
            />
          </Stack.Item>
        </Stack>
      </ResourceList.Item>
    );
  }
}