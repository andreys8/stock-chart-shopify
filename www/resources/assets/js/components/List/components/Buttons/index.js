'use strict';

import React, { Component } from 'react';
import * as PropTypes from 'prop-types';
import { Button, ButtonGroup, Icon } from '@shopify/polaris';
import EditIcon from './edit.svg';
import PlayIcon from './play.svg';
import PauseIcon from './pause.svg';
import { PolarisContext } from '@spurit/js-core';

export default class Buttons extends Component {
  /**
   * @param props
   */
  constructor (props) {
    super(props);
    this.state = {
      edit: {
        loading: false,
      },
      enabled: {
        loading: false,
      },
      remove: {
        loading: false,
      }
    };
    this.onEnabled = props.onEnabled;
    this.onRemove = props.onRemove;
    this.onEdit = props.onEdit;
  }

  /**
   *
   * @returns {{onEdit: onEdit, visible: {edit: boolean, enabled: boolean, remove: boolean}, alert: {message: string}, onEnabled: onEnabled, onRemove: onRemove, items: {edit: {disabled: boolean, loading: boolean}, enabled: {disabled: boolean, loading: boolean}, remove: {disabled: boolean, loading: boolean}}, enabled: boolean}}
   */
  static get defaultProps () {
    return {
      enabled: true,
      items: {
        enabled: {
          loading: false,
          disabled: false
        },
        remove: {
          loading: false,
          disabled: false
        },
        edit: {
          loading: false,
          disabled: false
        },
      },
      visible: {
        enabled: true,
        remove: true,
        edit: true
      },
      size: 'medium',
      alert: {
        message: 'Do you want to delete the current offer?'
      },
      onEnabled: () => {},
      onRemove: () => {},
      onEdit: () => {},
    };
  }

  /**
   *
   * @returns {{onEdit: Requireable<(...args: any[]) => any>, visible: Requireable<InferProps<{edit: Requireable<boolean>, enabled: Requireable<boolean>, remove: Requireable<boolean>}>>, alert: Requireable<InferProps<{message: Requireable<string>, title: Requireable<string>}>>, onEnabled: Requireable<(...args: any[]) => any>, onRemove: Requireable<(...args: any[]) => any>, items: Requireable<InferProps<{edit: Requireable<InferProps<{disabled: Requireable<boolean>, loading: Requireable<boolean>}>>, enabled: Requireable<InferProps<{disabled: Requireable<boolean>, loading: Requireable<boolean>}>>, remove: Requireable<InferProps<{disabled: Requireable<boolean>, loading: Requireable<boolean>}>>}>>, enabled: Requireable<boolean>}}
   */
  static get propTypes () {
    return {
      items: PropTypes.shape({
        enabled: PropTypes.shape({
          loading: PropTypes.bool,
          disabled: PropTypes.bool,
        }),
        remove: PropTypes.shape({
          loading: PropTypes.bool,
          disabled: PropTypes.bool,
        }),
        edit: PropTypes.shape({
          loading: PropTypes.bool,
          disabled: PropTypes.bool,
        }),
      }),
      visible: PropTypes.shape({
        enabled: PropTypes.bool,
        remove: PropTypes.bool,
        edit: PropTypes.bool,
      }),
      alert: PropTypes.shape({
        message: PropTypes.string,
        title: PropTypes.string,
      }),
      size: PropTypes.string,
      enabled: PropTypes.bool,
      onEnabled: PropTypes.func,
      onRemove: PropTypes.func,
      onEdit: PropTypes.func,
    };
  }

  /**
   *
   * @param params
   */
  set disabled (params) {
    let state = this.state;
    for (let type in params) {
      if (params.hasOwnProperty(type)) {
        state[type].loading = params[type];
        state[type].disabled = params[type];
      }
    }
    this.setState(state);
  }

  /**
   *
   * @param type
   * @returns {boolean}
   */
  visible (type) {
    let visible = {...Buttons.defaultProps.visible, ...this.props.visible};
    if (visible.hasOwnProperty(type)) {
      return visible[type];
    }
    return false;
  }

  /**
   *
   * @return {*}
   */
  get iconEnabled () {
    let state = this.state;
    let enabled = this.props.enabled;
    if (!state.enabled.loading) {
      if (enabled) {
        return <img alt={null} src={PauseIcon}/>;
      }
      return <img alt={null} src={PlayIcon}/>;
    }
    return null;
  }

  /**
   *
   * @return {*}
   */
  get iconDelete () {
    let state = this.state;
    if (state.remove.loading) {
      return null;
    }
    return <Icon source="delete"/>;
  }

  /**
   *
   * @return {*}
   */
  get editButton () {
    const {size} = this.props;
    return this.visible('edit') && <Button
      size={size}
      onClick={evt => {
        evt.stopPropagation();
        this.onEdit();
      }}
    >
      <span style={{
        backgroundImage: `url(${EditIcon})`,
        width: '19px',
        height: '19px',
        display: 'block',
        padding: 0,
        position: 'absolute',
        top: 0,
        bottom: 0,
        right: 0,
        left: '-9px',
        margin: 'auto',
        backgroundSize: '85%',
        backgroundPosition: 'center',
        backgroundRepeat: 'no-repeat'
      }}
      />
    </Button>;
  }

  /**
   *
   * @return {*}
   */
  get enabledButton () {
    const props = this.props;
    let state = this.state;
    return this.visible('enabled') && <Button
      icon={this.iconEnabled}
      primary={!props.enabled}
      loading={state.enabled.loading}
      size={props.size}
      disabled={state.enabled.disabled}
      onClick={evt => {
        evt.stopPropagation();
        this.disabled = {enabled: true};
        this.onEnabled(() => {
          this.disabled = {enabled: false};
        });
      }}
    />;
  }

  /**
   *
   * @return {*}
   */
  get removeButton () {
    const props = this.props;
    let state = this.state;
    return this.visible('remove') && (
      <Button
        destructive={true}
        loading={state.remove.loading}
        disabled={state.remove.disabled}
        icon={this.iconDelete}
        size={props.size}
        onClick={evt => {
          evt.stopPropagation();
          PolarisContext.bar.Modal.alert({
            children: props.alert.message,
            cancelContent: 'Cancel',
            confirmContent: 'Delete',
            destructive: true,
            onClose: () => {
            },
            onConfirm: () => {
              this.disabled = {remove: true};
              this.onRemove(() => {
                this.disabled = {remove: false};
              });
            }
          });
        }}
      />
    );
  }

  /**
   *
   * @return {*}
   */
  render () {
    return (
      <ButtonGroup segmented>
        {this.enabledButton}
        {this.editButton}
        {this.removeButton}
      </ButtonGroup>
    );
  }
}