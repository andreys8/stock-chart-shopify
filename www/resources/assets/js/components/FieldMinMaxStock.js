import React, {Component, Fragment } from "react";
import { TextField } from "@shopify/polaris";

export default class FieldMinMaxStock extends Component {

    /**
     *
     * @param props
     */
    constructor(props) {
        super(props);
        this.handleChangeHigh = this.props.handleChangeHigh;
        this.handleChangeLow = this.props.handleChangeLow;
    }

    render() {
        return (
            <Fragment>
                <TextField
                    id={"maxValue"}
                    label="Max product stock"
                    type="number"
                    min={1}
                    value={parseInt(this.props.maxValue)}
                    onChange={this.handleChangeHigh}
                />
                <TextField
                    id={"minValue"}
                    label="Min product stock"
                    type="number"
                    min={0}
                    value={parseInt(this.props.minValue)}
                    onChange={this.handleChangeLow}
                />
            </Fragment>
        );
    }
}
