'use strict';
import "core-js/stable";
import "regenerator-runtime/runtime";

import React from 'react';
import {render} from 'react-dom';
import {SpuritApp} from '@spurit/js-core';

import {routes, PAYMENT, INDEX} from './routes';
import topMenu from './top-menu';
import Storage from './Storage';
import enTranslations from '@shopify/polaris/locales/en.json';

(() => {
  let {shop, clientId, embedded, is_paid, ...otherProps} = window.spurit;
  render(
    <SpuritApp
      shop={shop}
      i18n={enTranslations}
      shopifyApiKey={clientId}
      embedded={embedded}
      debug={otherProps.app_env === 'local'}
      icon={document.location.protocol + '//' + document.location.hostname + '/img/icons/spurit-logo-icon.png'}
      isPaid={is_paid /*||true*/}

      routes={routes}
      topMenu={topMenu}
      footer={null}

      mainPageUrl={INDEX}
      paymentPageUrl={PAYMENT}

      {...otherProps}
      request={{test: "test"}}
      storage={new Storage(window.spurit.initial)}
    />,
    document.getElementById('app')
  );
})();
