import {observable, toJS} from 'mobx';
import {API} from './routes';
import {Toast} from '@shopify/app-bridge/actions';
import Settings from "./pages/Settings/Settings";
import Request from "./Request";
const TOAST_DURATION = 5000;

class Storage {
  /**
   * @type {Array}
   * @private
   */
  @observable _ids = [];

    /**
     *
     */
  @observable settings = {high_quantity: 1, low_quantity: 0, show_real_quantity: false};

    /**
     *
     * @type {Array}
     */
  @observable products = [];

  /**
   * @type {Object}
   */
  @observable urlParams = {};

  /**
   * @param {{}} data
   * @param {{}} app
   */
  constructor(data, app) {
    this.app = app;
    this._ids = data.ids;
    this.settings = data.settings;
    this.urlParams = data.urlParams;
    this.save = this.save.bind(this);
    this.getData = this.getData.bind(this);
  }

  /**
   * @param context
   */
  set context(context) {
    this._context = context;
    Request.context = context;
  }

    /**
     *
     * @param url
     * @param params
     * @param callback
     */
  save(url, params, callback = () => null) {
      Request.post(url, params, (data) => {
          Request.showMessage('Success');
          callback(data)
      });
  }

    /**
     *
     * @param url
     * @param params
     * @param callback
     */
  getData(url, params, callback = () => null) {
      Request.get(url, params, (data) => {
          callback(data);
      })
  }
}

export default Storage;
