'use strict';

import Settings from './pages/Settings/Settings';
import FAQPage from './pages/FAQ/FAQ';
import {Payment} from './pages/Payment';
import Dashboard from './pages/Dashboard/Dashboard';
import {navigateTo} from "@spurit/js-core";
import New from './pages/Products/new'

const INDEX = '/';
const FAQ = '/faq';
const PAYMENT = '/payment';
const SETTINGS = '/settings';

const PRODUCTS = {
    MAIN: INDEX,
    CREATE: '/product/new',
    EDIT: '/product/:id',
    INDEX: '/api/product',      // GET
    STORE: '/api/product',      // POST
    SHOW: '/api/product/:id',   // GET
    UPDATE: '/api/product/:id', // PUT|PATCH
    DESTROY: '/api/product/:id' // DELETE
};

const API = {
  PRODUCTS: '/api/products',
  ITEMS_PAGE: '/api/products/page/:id',
  PC: {
    NOTIFY: '/api/pc/notify'
  },
  SETTINGS: {
    SAVE: {
        FULL: '/settings/save/'
    }
  },
};

const routes = [
  {
    path: INDEX,
    exact: true,
    page: {
      component: Dashboard,
      title: 'Dashboard',
      primaryAction: {
        content: 'Add product settings',
        onAction: (design) => navigateTo(PRODUCTS.CREATE)
      }
    }
  },
  {
    path: PRODUCTS.CREATE,
    exact: true,
    page: {
      component: New,
      title: 'New Product',
      breadcrumbs: [
        {
          content: 'Product Dashboard',
          onAction: () => navigateTo(PRODUCTS.MAIN)
        }
      ],
      primaryAction: {
        content: 'Save',
        onAction: (option) => {
          option.save(() => navigateTo(PRODUCTS.MAIN));
        }
      }
    }
  },
  {
    path: PRODUCTS.EDIT,
    exact: true,
    page: {
       component: New,
       title: 'Edit Product',
       breadcrumbs: [
         {
           content: 'Product Dashboard',
           onAction: () => navigateTo(PRODUCTS.MAIN)
         }
       ],
       primaryAction: {
         content: 'Save',
         onAction: (option) => {
           option.save(() => navigateTo(PRODUCTS.MAIN));
         }
       }
    }
  },
  {
    path: SETTINGS,
    page: {
      component: Settings,
      title: 'Settings'
    }
  }
];
export {routes, INDEX, FAQ, PAYMENT, SETTINGS, API, PRODUCTS};
