'use strict';

import React, { Component } from 'react';
import { SelectorPicker } from '@spurit/js-selector-picker';
import { Card } from '@shopify/polaris';
import * as PropTypes from 'prop-types';

export default class Selector extends Component {

  /**
   *
   * @param props
   */
  constructor (props) {
    super(props);

    this.onChange = props.onChange;
  }

  /**
   *
   * @returns {{settings: {}, onChange: onChange, position: boolean, error: string, value: {selector: string, position: string}}}
   */
  static get defaultProps () {
    return {
      value: {
        position: 'after',
        selector: ''
      },
      settings:{},
      error: '',
      position: false,
      onChange: () => {}
    };
  }

  /**
   *
   * @returns {{settings: Requireable<InferProps<{}>>, onChange: Requireable<(...args: any[]) => any>, appName: Validator<NonNullable<string>>, name: Validator<NonNullable<string>>, position: Requireable<boolean>, error: Requireable<string>, value: Requireable<InferProps<{selector: Requireable<string>, position: Requireable<string>}>>, url: Validator<NonNullable<string>>}}
   */
  static get propTypes () {
    return {
      appName: PropTypes.string.isRequired,
      url: PropTypes.string.isRequired,
      name: PropTypes.string.isRequired,
      value: PropTypes.shape({
        position: PropTypes.oneOf(['after', 'before']),
        selector: PropTypes.string
      }),
      settings: PropTypes.shape({
        barMessage: PropTypes.string,
        barNotice: PropTypes.string,
        demoText: PropTypes.string
      }),
      error: PropTypes.string,
      position: PropTypes.bool,
      onChange: PropTypes.func
    };
  }

  /**
   *
   * @returns {Selector.defaultProps.value}
   */
  get value () {
    const {value} = this.props;
    return {...Selector.defaultProps.value, ...value};
  }

  get settings () {
    const {settings, title} = this.props;
    if (!settings.hasOwnProperty('barNotice')) {
      settings.barNotice = title;
    }
    return {...Selector.defaultProps.settings, ...settings};
  }

  /**
   *
   * @returns {string}
   */
  render () {
    const {appName, name, url, title, position, error} = this.props;
    return (
      <Card.Section
        title={title}
      >
        <SelectorPicker
          label=''
          helpText=''
          title='Select'
          error={error}
          sign={appName}
          url={url}
          visible={false}
          position={position}
          value={this.value}
          settings={this.settings}
          onChange={value => {
            this.onChange(name, value);
          }}
        />
      </Card.Section>
    );
  }
}
