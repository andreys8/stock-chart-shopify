'use strict';

import React, { Component } from 'react';
import {Layout, Card, Link, FormLayout, TextField, Button, ResourceList, ResourceItem, Avatar, TextStyle} from '@shopify/polaris';
import CheckBoxStock from "../../components/CheckBoxStock";
import FieldMinMaxStock from "../../components/FieldMinMaxStock";
import {observer} from "mobx-react";

import Selector from './components/Selector';
import * as PropTypes from 'prop-types';

@observer class Settings extends Component {

    /**
     *
     * @param props
     */
    constructor(props) {
        super(props);
        this.storage = props.storage;
        this.state = {
            settings: props.storage.settings,
            loading: false,
            errors: {},
            shopUrl: props.shop.shopUrl || 'https://' + props.shop.domain,
            saveUrl: '/admin/settings/save'
        };
        this.saveSettings = this.saveSettings.bind(this);
        this.setSelector = this.setSelector.bind(this);
    }

    /**
     *
     * @returns {{app_name: *, config: *}}
     */
    static get propTypes () {
        return {
            app_name: PropTypes.string.isRequired,
            config: PropTypes.shape({
                selectorPicker: PropTypes.shape({
                    productPage: PropTypes.string.isRequired
                }).isRequired
            }).isRequired
        };
    }

    /**
     *
     * @param high_quantity
     */
    handleChangeHigh = high_quantity => {
        let {settings} = this.state;
        settings.high_quantity = parseInt(high_quantity);
        if(settings.high_quantity > settings.low_quantity) {
            this.setState({settings})
        }
    };

    /**
     *
     * @param low_quantity
     */
    handleChangeLow = low_quantity => {
        let {settings} = this.state;
        settings.low_quantity = parseInt(low_quantity);
        if(settings.low_quantity < settings.high_quantity) {
            this.setState({settings})
        }
    };

    /**
     *
     * @returns {*[]}
     */
    get selectors () {
        const {selectorPicker} = this.props.config;
        return [
            {
                title: 'Select progress bar position',
                url: selectorPicker.productPage,
                name: 'bar'
            }
        ];
    }

    /**
     *
     * @param name
     * @param selector
     */
    setSelector (name, selector) {
        let {settings, errors} = this.state;
        if  (selector.selector.length) {
            settings.selectors = {...settings.selectors, ...{[name]: selector}};
        } else {
            delete settings.selectors[name];
        }
        if (errors.hasOwnProperty('selectors.' + name + '.selector')) {
            errors['selectors.' + name + '.selector'] = false;
        }
        this.setState({settings, errors});
    }

    /**
     *
     * @param name
     * @returns {{}|*}
     */
    getSelector (name) {
        const {selectors} = this.state.settings;
        if (selectors.hasOwnProperty(name)) {
            return selectors[name];
        }
        return {};
    }


    /**
     *
     * @param name
     */
    getError (name) {
        const {errors} = this.state;
        if (errors.hasOwnProperty('selectors.' + name + '.selector')) {
            return errors['selectors.' + name + '.selector'] ? 'This selector is required' : '';
        }
        return '';
    }

    saveSettings() {
        this.setState({loading: true});
        const data = this.state.settings;
        const url  = this.state.saveUrl;
        this.storage.save(url, data, () => {
            this.setState({loading: false})
        });
    }

    /**
     *
     * @param show_real_quantity
     */
    onChecked = show_real_quantity => {
        let {settings} = this.state;
        settings.show_real_quantity = show_real_quantity;
        this.setState({settings})
    };


    render() {
        const { app_name, storage: {settings} } = this.props;
        const { loading } = this.state;
        const { low_quantity, high_quantity, show_real_quantity } = this.state.settings;
        return (
            <Layout>
                <Layout.Section>
                    <FieldMinMaxStock
                        handleChangeHigh={this.handleChangeHigh}
                        handleChangeLow={this.handleChangeLow}
                        maxValue={high_quantity}
                        minValue={low_quantity}
                    />
                    <CheckBoxStock
                        onChange={this.onChecked}
                        checked={show_real_quantity}
                    />
                </Layout.Section>
                <Layout.Section>
                    {this.selectors.map((selector, key) => {
                        return <Selector
                            key={key}
                            appName={app_name}
                            error={this.getError(selector.name)}
                            {...selector}
                            value={this.getSelector(selector.name)}
                            onChange={this.setSelector}
                        />;
                    })}
                </Layout.Section>
                <Layout.Section>
                    <Button primary loading={loading} onClick={this.saveSettings}>Save</Button>
                </Layout.Section>
            </Layout>
        );
    }
}

export default Settings;
