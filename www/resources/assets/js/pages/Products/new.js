'use strict';

import React, { Component } from 'react';
import {Layout, Card, Link, FormLayout, TextField, Button, ResourceList, ResourceItem, Avatar, TextStyle} from '@shopify/polaris';
import {ProductSelector} from "@spurit/js-product-selector";
import FieldMinMaxStock from "../../components/FieldMinMaxStock";
import CheckBoxStock from "../../components/CheckBoxStock"

class New extends Component {

    constructor(props) {
        super(props);
        this.storage = props.storage;

        this.state = {
            settings: props.storage.settings,
            products : props.storage.products,
            loading: false,
            shopUrl: props.shop.shopUrl || 'https://'+ props.shop.domain,
            saveUrl: '/admin/products/save'
        };
        this.saveSettings = this.saveSettings.bind(this);
    }

    updateField(field) {
        return (value) => this.setState({[field]: value});
    }

    /**
     *
     * @param high_quantity
     */
    handleChangeHigh = high_quantity => {
        let {settings} = this.state;
        settings.high_quantity = parseInt(high_quantity);
        if(settings.high_quantity > settings.low_quantity) {
            this.setState({settings})
        }
    };

    /**
     *
     * @param low_quantity
     */
    handleChangeLow = low_quantity => {
        let {settings} = this.state;
        settings.low_quantity = parseInt(low_quantity);
        if(settings.low_quantity < settings.high_quantity) {
            this.setState({settings})
        }
    };

    saveSettings() {
        this.setState({loading: true});
        const data = {
            ids: this.state.productsId,
            settings: this.state.settings,
        };
        const url  = this.state.saveUrl;
        this.storage.save(url, data, (response) => {
            this.setState({loading: false})
        });
    }

    /**
     *
     * @param show_real_quantity
     */
    onChecked = show_real_quantity => {
        let {settings} = this.state;
        settings.show_real_quantity = show_real_quantity;
        this.setState({settings})
    };

    render() {
        const { loading } = this.state;
        const { low_quantity, high_quantity, show_real_quantity } = this.state.settings;
        return (
            <Layout>
                <Card>
                    <Card.Section>
                        <ProductSelector
                            selected={this.state.productsId}
                            bottomText="App will apply to {items}"
                            onChange={(ids) => {
                                this.setState({productsId: ids});
                            }}
                        />
                    </Card.Section>
                    <Card.Section>
                        <FieldMinMaxStock
                            handleChangeHigh={this.handleChangeHigh}
                            handleChangeLow={this.handleChangeLow}
                            maxValue={high_quantity}
                            minValue={low_quantity}
                        />
                        <CheckBoxStock
                            onChange={this.onChecked}
                            checked={show_real_quantity}
                        />
                    </Card.Section>
                    <Card.Section>
                        <Button primary loading={loading} onClick={this.saveSettings}>Save</Button>
                    </Card.Section>
                </Card>
            </Layout>
        );
    }
}

export default New;
