'use strict';

import React, { Component } from 'react';
import {Layout} from '@shopify/polaris';
import questions from './components/Questions'
import QA from './components/QA'
import {SelectorPicker} from '@spurit/js-selector-picker';

export default class FAQ extends Component {

    constructor(props) {
        super(props);
        console.log(props)
    }

    render() {
        return (
          <>
            <Layout>
                {questions.map((q,k)=>{
                    return <QA key={k} {...q}/>
                })}
            </Layout>
            <SelectorPicker
              label="Select product image on a product page"
              sign={'stock_chart_shopify'}
              url={'https://' + this.props.shop.domain_front + '/collections/all'}
              withoutUniqueId={true}
              visor={true}
              shopId={this.props.shop.user_id}
              value={'test'}
              onChange={selector => console.log(selector)}
            />
          </>
      );
    }
}
