'use strict';

import React from 'react';
import {Layout, Card} from '@shopify/polaris';

const QA = function (props) {
	return (
		<Layout.Section>
			<Card
				title={props.question}
				sectioned
			>
				{props.answer}
			</Card>
		</Layout.Section>
	);
};
export default QA;