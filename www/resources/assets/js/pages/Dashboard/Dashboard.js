'use strict';

import React, {Component} from 'react';
import * as PropTypes from 'prop-types';
import {
    Card,
    Thumbnail,
    ResourceList,
    ResourceItem,
    TextStyle,
    Heading,
    Pagination
} from '@shopify/polaris';

class Dashboard extends Component {

    constructor(props) {
        super(props);
        this.storage = props.storage;
        this.state = {
            shopUrl: props.shop.shopUrl || 'https://' + props.shop.domain,
            products: [],
            loading: true,
            url : "/admin/products/get",
            currentPage: 1,
            lastPage : 1,
            hasPrevious : false,
            hasNext : true
        };
    }

    updateField(field) {
        return (value) => this.setState({[field]: value});
    }

    componentDidMount() {
        let url = this.state.url;
        this.storage.getData(url, {}, (response) => {
            this.setState({products: response});
            if(response[0] !== undefined){
                this.setState({lastPage: response[0].lastPage});
            }
            this.setState({loading: false});
        });
    }

    render() {
        const { loading } = this.state;
        let state = this.state;
        return (
            <Card sectioned>
                <ResourceList loading={loading}
                    items={state.products}
                    renderItem={(item) => {
                        const {id, handle, title, image} = item.product;
                        const {high_quantity, low_quantity, show_real_quantity} = item.productOptions;
                        const media = <Thumbnail source={image.src} size={"medium"} alt={title}/>;
                        return (
                            <ResourceItem
                                id={id}
                                url={state.shopUrl + "/apps/stock-chart-dev/product/new"}
                                media={media}
                                accessibilityLabel={`View details for ${title}`}
                            >
                                <Heading>{title}</Heading>
                                <p><TextStyle variation="positive"> {high_quantity} </TextStyle></p>
                                <p><TextStyle variation="negative"> {low_quantity} </TextStyle></p>
                                <p>{show_real_quantity ? "Yes" : "No"}</p>
                            </ResourceItem>
                        );
                    }}
                />
                {this.state.lastPage > 1 &&
                <Pagination
                    hasPrevious={this.state.hasPrevious}
                    onPrevious={() => {
                        this.setState({loading: true});
                        let pager = { page: this.state.currentPage - 1 };

                        if (pager.page < 1) { pager.page = 1; }
                        if(pager.page < this.state.lastPage) {this.setState({hasNext: true})}
                        if (pager.page !== 1) {
                            this.setState({hasPrevious: true})
                        } else {
                            this.setState({hasPrevious: false})
                        }

                        this.storage.getData(this.state.url, pager, (response) => {
                            this.setState({products: response, currentPage: pager.page, loading: false});
                        });
                    }}
                    hasNext={this.state.hasNext}
                    onNext={() => {
                        this.setState({loading: true});
                        let pager = { page: this.state.currentPage + 1 };

                        if (pager.page !== 1) { this.setState({hasPrevious: true}) }
                        if (pager.page === this.state.lastPage) { this.setState({hasNext: false}) }

                        this.storage.getData(this.state.url, pager, (response) => {
                            this.setState({products: response, currentPage: pager.page, loading: false});
                        });
                    }}
                />
                }
            </Card>
        );
    }
}

export default Dashboard;
