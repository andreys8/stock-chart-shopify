'use strict';

import * as route from './routes';

export default [
  {
    content: 'Dashboard',
    url: route.INDEX
  },
  {
    content: 'Settings',
    url: route.SETTINGS,
    target: 'new'
  }
];
