import axios from 'axios';
import * as Qs from 'qs';
import {Toast} from '@shopify/app-bridge/actions';

export default class Request {

    static #context;

    static set context(context) {
        Request.#context = context;
    }

    static get context() {
        return Request.#context
    }
  /**
   *
   * @param error
   */
  static showError (error) {
    let errorMessage = error.message;
    if (error.response) {
      if (error.response.data.hasOwnProperty('message')) {
        errorMessage = error.response.data.message;
      }
    } else {
      console.error(error);
    }
      const toastNotice = Toast.create(Request.context, {
          message: errorMessage,
          isError: true,
          duration: 0
      });
      toastNotice.dispatch(Toast.Action.SHOW);
  }

  /**
   *
   * @param message
   */
  static showMessage (message) {
      const toastNotice = Toast.create(Request.context, {
          message: message,
          isError: false,
          duration: 0
      });
      toastNotice.dispatch(Toast.Action.SHOW);
  }

  /**
   *
   * @param url
   * @param params
   * @param callback
   * @param historyRoute
   */
  static get (url, params = {}, callback = () => null, historyRoute = null) {
    axios.get(url, {
      params: params,
      paramsSerializer: function (params) {
        return Qs.stringify(params, {arrayFormat: 'indices'});
      }
    })
      .then(response => {
        callback(response.data);
        this.routeHistory(historyRoute, params);
      })
      .catch(
        error => {
          this.showError(error);
        }
      );
  }

  /**
   *
   * @param url
   * @param params
   * @param callback
   * @param errorCallback
   */
  static put (url, params = {}, callback = () => null, errorCallback = () => null) {
    axios.put(url, params)
      .then(response => {
        callback(response.data);
      })
      .catch(error => {
        this.showError(error);
        if (error.response) {
          errorCallback(error.response.data);
        }
      });
  }

  /**
   *
   * @param url
   * @param params
   * @param callback
   * @param errorCallback
   */
  static post (url, params = {}, callback = () => null, errorCallback = () => null) {
    axios.post(url, params)
      .then(response => {
        callback(response.data);
      })
      .catch(error => {
        this.showError(error);
        if (error.response) {
          errorCallback(error.response.data);
        }
      });
  }

  /**
   *
   * @param url
   * @param callback
   * @param errorCallback
   */
  static delete (url, callback = () => null, errorCallback = () => null) {
    axios.delete(url)
      .then(response => {
        callback(response.data);
      })
      .catch(error => {
        this.showError(error);
        if (error.response) {
          errorCallback(error.response.data);
        }
      });
  }

  /**
   *
   * @param url
   * @param newParams
   */
  static routeHistory (url = null, newParams = {}) {
    if (url) {
      let params = {};
      for (let param in newParams) {
        if (newParams.hasOwnProperty(param)) {
          if (newParams[param].length || newParams[param] > 1) {
            params[param] = newParams[param];
          }
        }
      }
    }
  }
}
