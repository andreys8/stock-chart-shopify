/**
 * @constructor
 */
export let Env = function () {
  /**
   * @type {string}
   */
  this.appName = process.env.MIX_APP_NAME;

  /**
   * @type {string}
   */
  this.awsCommonPath = process.env.MIX_AWS_COMMON_PATH.replace(/\/+$/, '') + '/';

  /**
   * @type {string}
   */
  this.awsStorePath = process.env.MIX_AWS_STORE_PATH.replace(/\/+$/, '') + '/';

  /**
   * @type {string}
   */
  this.appApiUrl = process.env.MIX_APP_API_URL;
};