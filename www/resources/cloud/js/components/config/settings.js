let _ = require('lodash');

/**
 * @param {string} appName
 * @constructor
 */
export let Settings = function (appName) {
  let defaultSettings = {};

  /**
   *
   */
  this.init = function () {
    let isSettingsSet = (
      typeof(Spurit) !== 'undefined' &&
      typeof(Spurit[appName]) !== 'undefined' &&
      typeof(Spurit[appName].settings) !== 'undefined'
    );
    let settings = defaultSettings;
    if (isSettingsSet) {
      settings = _.defaultsDeep(_.cloneDeep(Spurit[appName].settings), Spurit[appName].settings);
    }
    for (let key in settings) {
      this[key] = settings[key];
    }
  };
};