let _ = require('lodash');

/**
 * @param {string} appName
 * @constructor
 */
export let Customization = function (appName) {
  let defaultCustomization = {
    onConfigReady: (config) => null,
    onListenerReady: (listener) => null,
  };

  let isCustomizationSet = (
      typeof(Spurit) !== 'undefined' &&
      typeof(Spurit[appName]) !== 'undefined' &&
      typeof(Spurit[appName].customization) !== 'undefined'
  );
  let customization = defaultCustomization;
  if (isCustomizationSet) {
    customization = _.defaultsDeep(Spurit[appName].customization, customization);
  }
  for (let key in customization) {
    this[key] = customization[key];
  }
};