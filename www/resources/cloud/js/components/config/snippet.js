let _ = require('lodash');

/**
 * @param {string} appName
 * @constructor
 */
export let Snippet = function (appName) {
  let defaultSnippet = {
    appId: 0,
    shopHash: '',
    moneyFormat: '{{amount_with_comma_separator}}$',
  };

  let isSnippetSet = (
      typeof(Spurit) !== 'undefined' &&
      typeof(Spurit[appName]) !== 'undefined' &&
      typeof(Spurit[appName].snippet) !== 'undefined'
  );
  let snippet = defaultSnippet;
  if (isSnippetSet) {
    snippet = _.defaultsDeep(Spurit[appName].snippet, snippet);
  }
  for (let key in snippet) {
    this[key] = snippet[key];
  }
};