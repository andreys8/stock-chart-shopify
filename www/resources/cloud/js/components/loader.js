'use strict';

/**
 * @param {AppLibrary} library
 * @param {string} appName
 * @constructor
 */
export let Loader = function (library, appName) {

  /**
   * @type string
   */
  let currentPage = library.getCurrentPage();

  /**
   * @type {string}
   */
  let loadJQuery = '';

  /**
   * @type {{selectorPicker: function|bool|null, appResources: [{url: string, pages: [string]}]}}
   */
  let params = {
    selectorPicker: null,
    appResources: []
  };

  /**
   * @param {string} min
   * @param {string} recommended
   */
  this.requireJQuery = function (min, recommended = '') {
    if (!recommended) {
      recommended = min;
    }
    // todo: Корректно сравнить версии
    if (typeof jQuery === 'undefined' || parseFloat(jQuery.fn.jquery) < parseFloat(min)) {
      loadJQuery = recommended;
    }
  };

  /**
   * @param {function(string)|bool} callback
   *        Function to check if SP should start in the current page (will be passed to that func.) or true to start SP unconditionally
   */
  this.enableSelectorPicker = function (callback) {
    params.selectorPicker = callback
  };

  /**
   * @param {string} url
   * @param {[string]} pages
   */
  this.addResources = function (url, pages = []) {
    if (!Array.isArray(pages)) {
      pages = [pages];
    }
    params.appResources.push({
      url: url,
      pages: pages
    });
  };

  /**
   * @param {function(jQuery, string)} appEntryPoint
   */
  this.run = function (appEntryPoint) {
    try {
      if (loadJQuery !== '') {
        library.loadStatic(
            '//ajax.googleapis.com/ajax/libs/jquery/' + loadJQuery + '/jquery.min.js',
            function () {
              let $ = jQuery.noConflict(true);
              runApp($, appEntryPoint);
            }
        );
      } else {
        runApp(typeof(jQuery) !== 'undefined' ? jQuery : null, appEntryPoint);
      }
    } catch (e) {
      throw new Error('Exception while loading jQuery: ' + e.message);
    }
  };

  /**
   * @param {jQuery|null} $
   * @param {function(jQuery, string)} appEntryPoint
   */
  let runApp = function ($, appEntryPoint) {
    let _GET = library.requestGetVars();
    if (params.selectorPicker && typeof(_GET.sign) !== 'undefined' && _GET.sign === appName) {
      if (typeof(params.selectorPicker) !== 'function' || params.selectorPicker(currentPage)) {
        library.loadStatic(_GET.script);
        //library.loadStatic('https://s3.amazonaws.com/shopify-apps/Plugins/SelectorPicker/selector-picker-3.8.2-dev1.js');
      }
      return;
    }

    library.loadStatic(
        getResources(currentPage),
        () => {
          library.onLoad(() => {
            // Run App
            appEntryPoint($, currentPage);
          });
        }
    );
  };

  /**
   * @param {string} page
   * @return {[string]}
   */
  let getResources = function (page) {
    let resources = [];
    for (let i = 0; i < params.appResources.length; i++) {
      let resource = params.appResources[i];
      if (!resource.pages.length || resource.pages.indexOf(page) !== -1 || resource.pages.indexOf('all') !== -1) {
        resources.push(resource.url);
      }
    }
    return resources;
  };
};
