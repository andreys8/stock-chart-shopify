import axios from 'axios';
import {Env} from "./config/env";
import ProgressBar from "progressbar.js";
/**
 * @param {jQuery} $
 * @param {AppConfig} config
 * @param {AppLibrary} library
 * @param {string} page
 * @constructor
 */
export let Application = function ($, config, library, page) {
    if(page === 'products') {
        let env = new Env(),
            globalSettingsLow = Spurit.stock_chart_shopify.settings.low_quantity,
            globalSettingsHigh = Spurit.stock_chart_shopify.settings.high_quantity,
            globalSettingsQty = Spurit.stock_chart_shopify.settings.show_real_quantity,
            productSettings = Spurit.stock_chart_shopify.productSettings,
            selector = Spurit.stock_chart_shopify.settings.selectors.bar.selector,
            variantsQty = library.snippet.variantQty,
            variantsQtyLength = Object.keys(variantsQty).length,
            firstVariantValue = Object.values(variantsQty)[0],
            progressBarElement = document.createElement('div'),
            scriptElement = document.createElement('script'),
            showQtyElement = document.createElement('div'),
            variantUrl = window.location.search.replace(/[^0-9]/g, ''),
            productForms = document.querySelectorAll('.single-option-selector');

        scriptElement.src = env.awsStorePath + library.snippet.shopHash + '.js';
        document.getElementsByTagName('head')[0].appendChild(scriptElement);

        progressBarElement.setAttribute('id', 'progressContainer');
        progressBarElement.style.cssText = ' width: 180px; height: 5px; margin: 10px';
        document.querySelector(selector).append(progressBarElement);

        /**
         *
         * @param high
         * @param low
         * @param value
         * @returns {string}
         */
        function setColor(high, low, value = firstVariantValue) {
            let color;
            if (value > high) color = 'green';
            else if (value <= low) color = 'red';
            else color = 'orange';
            return color;
        }

        /**
         *
         * @param color
         * @returns {number}
         */
        function checkProgress(color) {
            let progress;
            switch (color) {
                case 'green': progress = 1;
                    break;
                case 'orange': progress = 0.66;
                    break;
                case 'red': progress = 0.33;
                    break;
            }
            return progress;
        }

        /**
         *
         * @param color
         */
        function progressBar(color) {
            if(progressBarElement.childNodes.length === 1) {
                progressBarElement.removeChild(progressBarElement.childNodes[0]);
            }
            let bar = new ProgressBar.Line(progressBarElement, {
                strokeWidth: 4,
                easing: 'easeInOut',
                duration: 1400,
                color: color || '#FFF',
                trailColor: '#EEE',
                trailWidth: 100,
                svgStyle: {width: '100%', height: '100%'}
            });
            setTimeout(() => {
                bar.animate(checkProgress(color));
            }, 300);
        }

        /**
         *
         * @param showQty
         * @param qty
         */
        function appendQty(showQty, qty = firstVariantValue) {
            if(showQty) {
                showQtyElement.textContent = 'stock: ' + qty;
                document.querySelector(selector).append(showQtyElement);
            }
        }

        /**
         *
         * @param high
         * @param low
         * @param settingsQty
         */
        function changeVariant(high, low, settingsQty = globalSettingsQty) {
            for (let i = 0; i < productForms.length; i++) {
                let productForm = productForms[i];
                productForm.addEventListener('change', () => {
                    let variantUrl = window.location.search.replace(/[^0-9]/g, '');
                    appendQty(settingsQty, variantsQty[variantUrl]);
                    progressBar(setColor(high, low, variantsQty[variantUrl]));
                });
            }
        }

        axios.get(window.location.origin + window.location.pathname + '.js')
            .then((response) => {
                let checkGlobalSettings = true,
                    productId = response.data.id;
                for (let value in productSettings) {
                    if (productSettings.hasOwnProperty(value)) {
                        if (productId === productSettings[value].product_id) {
                            if (variantsQtyLength > 1) {
                                setColor(productSettings[value].config.high_quantity, productSettings[value].config.low_quantity, variantsQty[variantUrl]);
                                changeVariant(productSettings[value].config.high_quantity, productSettings[value].config.low_quantity, productSettings[value].config.show_real_quantity);
                                appendQty(productSettings[value].config.show_real_quantity, variantsQty[variantUrl]);
                            } else {
                                setColor(productSettings[value].config.high_quantity, productSettings[value].config.low_quantity);
                                appendQty(productSettings[value].config.show_real_quantity);
                            }
                            checkGlobalSettings = false;
                            break;
                        }
                    }
                }
                if(checkGlobalSettings) {
                    if (variantsQtyLength > 1) {
                        progressBar(setColor(globalSettingsHigh, globalSettingsLow, variantsQty[variantUrl]));
                        appendQty(globalSettingsQty, variantsQty[variantUrl]);
                        changeVariant(globalSettingsHigh, globalSettingsLow);
                    } else {
                        progressBar(setColor(globalSettingsHigh, globalSettingsLow));
                        appendQty(globalSettingsQty);
                    }
                }
            })
            .catch((error) => {
                console.log(error)
            });
    }
};
