#!/usr/bin/env bash

if [ -d "/vagrant/www/vendor" ]
then
    flock -n /tmp/webhooks-proxy-receive.lock php /vagrant/www/artisan webhooks:proxy-receive
fi