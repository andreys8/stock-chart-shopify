#!/usr/bin/env bash

chmod -R 777 /vagrant/www/storage
chmod -R 777 /vagrant/www/bootstrap/cache

# Bitbucket NPM auth
if ! [ -f "/home/vagrant/.npmrc" ]; then
  echo '//registry.npmjs.org/:_authToken=7805ebf9-cd55-4f4b-b83c-b0e6b79b4010' > /home/vagrant/.npmrc
fi
# END Bitbucket NPM auth

# Bitbucket Composer auth
bitbucketKey="dXsNFTU2TKGm2eUMPQ"
bitbucketSecret="c2wKQNKzz6XHLp26EUwWsa5LK5DzBbEK"

mkdir -p "/home/vagrant/.composer/"
if ! [ -f "/home/vagrant/.composer/auth.json" ]; then
  touch /home/vagrant/.composer/auth.json
fi
echo '{"bitbucket-oauth":{"bitbucket.org":{"consumer-key":"'$bitbucketKey'","consumer-secret":"'$bitbucketSecret'"}}}' > /home/vagrant/.composer/auth.json
chown vagrant:vagrant -R /home/vagrant/.composer
# END Bitbucket Composer auth

INSTALL=0
if ! [ -f "/vagrant/www/.env" ]; then
  INSTALL=1
  cp "/vagrant/www/.env.example" "/vagrant/www/.env"
fi

if ! [ -d "/vagrant/www/vendor" ]; then
    cd /vagrant/www && composer install --no-ansi
fi

if [ "$INSTALL" -eq 1 ]; then
    cd /vagrant/www && php artisan key:generate
fi

if ! [ -f "/vagrant/www/config/core.php" ]; then
    cd /vagrant/www && php artisan vendor:publish --provider="Spurit\Core\CoreServiceProvider"
fi

cd /vagrant/www && php artisan migrate
cd /vagrant/www && php artisan db:seed

cd /vagrant/www && php artisan ide-helper:generate
#cd /vagrant/www && php artisan ide-helper:models -n
cd /vagrant/www && php artisan ide-helper:meta

if ! [ -d "/vagrant/www/node_modules" ]; then
    cd /vagrant/www && yarn install --no-optional && yarn dev
fi

{ echo "* * * * * /vagrant/webhooks-proxy.sh"; } | crontab -
